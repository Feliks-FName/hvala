jQuery(function() {

    // Экранирование номера
    $( 'input[name="phone"]' ).inputmask({
        mask: '+7 999 999 99 99',
        clearIncomplete : true,
        definitions: {
            'X': {
                validator: '9'
            }
        }
    })

    // Отправка формы
    $( '.form' ).on( 'submit', function(e){
        let form = $(this)
        let data = form.serialize()
        let metrika = $(this).data('metrika')

        $.ajax({
            url: 'api/main.php',
            type: 'POST',
            data: data,
            success: function () {
                form[0].reset()
                $( '#thankslModal' ).modal( 'show' )
                // ym( metrika, 'reachGoal', 'goal1' )
            }
        })

        e.preventDefault()
    }) 

    // Получение get из url
    let getUrlParams = window
    .location
    .search
    .replace('?','')
    .split('&')
    .reduce(
        function(p,e){
            var a = e.split('=')
            p[ decodeURIComponent(a[0])] = decodeURIComponent(a[1])
            return p
        },
        {}
    )

    // Подстановка UTM-меток
    $( '.form input[name="utm_source"]' ).val( getUrlParams['utm_source'] )
    $( '.form input[name="utm_medium"]' ).val( getUrlParams['utm_medium'] )
    $( '.form input[name="utm_campaign"]' ).val( getUrlParams['utm_campaign'] )
    $( '.form input[name="utm_content"]' ).val( getUrlParams['utm_content'] )
    $( '.form input[name="utm_term"]' ).val( getUrlParams['utm_term'] )

    // Планируемый бюджет ползунок
    let output = $( '.rangeslider__output' )

    function updateOutput(el, val) {
        el.textContent = val
    }

    $( 'input[type="range"]' ).rangeslider({
        polyfill: false,
        onInit: function() {
            updateOutput(output[0], String(this.value).replace(/(\d)(?=(\d{3})+([^\d]|$))/g, '$1 ') + ' руб.')
        }
    })
    .on('input', function() {
        updateOutput(output[0], String(this.value).replace(/(\d)(?=(\d{3})+([^\d]|$))/g, '$1 ') + ' руб.')
    })

})
