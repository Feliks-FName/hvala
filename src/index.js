// JS
import './js/common'
import './js/district'

// SASS
import './assets/sass/main.sass'

// Bootstrap
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'

// Inputmask
import 'inputmask/dist/jquery.inputmask.min'

// Range slider
import 'rangeslider.js'