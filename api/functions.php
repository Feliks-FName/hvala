<?php

    class AddLead
    {
        
        // ------------------------------------ Определяем переменные ------------------------------------

        public $name;
        public $phone;
        public $email;

        public $qw1;
        public $qw2;
        public $qw3;
        public $qw4;
        public $qw5;
        public $qw6;
        public $qw7;
        public $qw8;

		public $utm_source;
		public $utm_medium;
		public $utm_campaign;
		public $utm_content;
		public $utm_term;
        
        public $msg;
        public $msgForManager;
        
        // ------------------------------------ Конструктор переменных ------------------------------------

        function __construct()
        {

            // Обрабатываем данные с формы

            $this -> name 			= $_POST['name'];
            $this -> phone 			= $_POST['phone'];
            $this -> email 			= $_POST['email'];
            $this -> district 		= $_POST['district'];

            $this -> qw2 		    = $_POST['qw2'];
            $this -> qw3 			= $_POST['qw3'];
            $this -> qw4 			= $_POST['qw4'];
            $this -> qw5			= $_POST['qw5'];
            $this -> qw6 			= $_POST['qw6'];
            // $this -> qw7 			= $_POST['qw7'];
            // $this -> qw8 			= $_POST['qw8'];
    
            $this -> utm_source		= $_POST['utm_source'];
            $this -> utm_medium		= $_POST['utm_medium'];
            $this -> utm_campaign	= $_POST['utm_campaign'];
            $this -> utm_content	= $_POST['utm_content'];
            $this -> utm_term		= $_POST['utm_term'];

            // Формируем сообщение для Email

            $this -> msg = "<p><strong>Имя: </strong> " . $this -> name . "</p>\r\n";
            $this -> msg .= "<p><strong>Телефон: </strong> " . $this -> phone . "</p>\r\n";
            $this -> msg .= "<p><strong>Email: </strong> " . $this -> email . "</p>\r\n";

            $this -> msg .= "<p><strong>Из какого вы города?: </strong> " . $this -> district . "</p>\r\n";
            $this -> msg .= "<p><strong>Есть ли у Вас опыт ведения бизнеса? </strong> " . $this -> qw2 . "</p>\r\n";
            $this -> msg .= "<p><strong>Какие услуги поддержки вы бы хотели получить? </strong> " . $this -> qw3 . "</p>\r\n";
            $this -> msg .= "<p><strong>Какой объем инвестиций для Вас комфортный? </strong> " . $this -> qw4 . "</p>\r\n";
            $this -> msg .= "<p><strong>Какие услуги вас интересуют? </strong> " . $this -> qw5 . "</p>\r\n";
            $this -> msg .= "<p><strong>Когда планируете начать? </strong> " . $this -> qw6 . "</p>\r\n";
           /*  $this -> msg .= "<p><strong>Планируемый бюджет: </strong> " . $this -> qw7 . "</p>\r\n";
            $this -> msg .= "<p><strong>Где вам удобно получать предложения?: </strong> " . $this -> qw8 . "</p>\r\n"; */

            $this -> msgForManager .= $this -> msg;
            $this -> msgForManager .= "<p><strong>utm_source:</strong> " . $this -> utm_source . "</p>\r\n";
            $this -> msgForManager .= "<p><strong>utm_medium:</strong> " . $this -> utm_medium . "</p>\r\n";
            $this -> msgForManager .= "<p><strong>utm_campaign:</strong> " . $this -> utm_campaign . "</p>\r\n";
            $this -> msgForManager .= "<p><strong>utm_content:</strong> " . $this -> utm_content . "</p>\r\n";
            $this -> msgForManager .= "<p><strong>utm_term:</strong> " . $this -> utm_term . "</p>\r\n";
        }

        // ------------------------------------ Отправка лида на почту ------------------------------------

        public function sendEmail($toSendCient, $toSendManager, $subject, $fromName, $fromEmail )
        {

            if ( !$this -> phone ) {
                exit();
            }

            echo $this -> msgForManager;
            
            $msg = $this -> msg;
            $msgForManager = $this -> msgForManager;

            $headers = "MIME-Version: 1.0\r\nContent-type: text/html; charset=utf-8\r\n";
            $headers .= "From: =?UTF-8?B?" . base64_encode( $fromName ) . "?= <" . $fromEmail . ">\r\n";

            if ( $toSendManager ) {
                mail( $toSendManager, "=?UTF-8?B?" . base64_encode( $subject ) . "?=", $msgForManager, $headers );
            }

            if ( $toSendCient ) {
                mail( $toSendCient, "=?UTF-8?B?" . base64_encode( $subject ) . "?=", $msg, $headers );
            }

        }

    }