<?php

	// Отключаем ошибки
	ini_set( 'error_reporting', E_ALL );
	ini_set( 'display_errors', 0 );
	ini_set( 'display_startup_errors', 0 );

	// Подключаем файл с функциями
	include( 'functions.php' );
	
	// Создаем объект класса AddLead
	$lead = new AddLead();

	// // Вызываем методы
	$lead -> sendEmail(
		false,
		'psquadkotov@yandex.ru, hvala.franchise@mail.ru',
		'Новый лид c сайта hvala.ru',
		'no-reply@hvala.franchise@mail.ru',
		'no-reply@hvala.franchise@mail.ru'
	);
